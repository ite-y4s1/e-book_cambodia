package com.example.e_bookcambodia


import androidx.fragment.app.Fragment

import androidx.fragment.app.FragmentManager

import androidx.fragment.app.FragmentTransaction

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.activity.ComponentActivity
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.e_bookcambodia.mvc.view.fragment.StoreFragment
import com.example.e_bookcambodia.theme.EBookCambodiaTheme

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        Handler().postDelayed({
            val intent = Intent(this, StoreFragment::class.java)
            startActivity(intent)
        }, 1000)


        // Show HomeFragment
        showFragment(StoreFragment())

        // Setup Listeners

        // Setup Listeners
//        binding.bottomNavigationView.setOnItemSelectedListener { item ->
//            if (item.getItemId() === R.id.mnuHome) {
//                showFragment(HomeFragment(username))
//            } else if (item.getItemId() === R.id.mnuProvinces) {
//                showFragment(ProvincesFragment())
//            } else if (item.getItemId() === R.id.mnuSearch) {
//                showFragment(SearchFragment())
//            } else if (item.getItemId() === R.id.mnuProfile) {
//                showFragment(ProfileFragment(username))
//            } else {
//                showFragment(MoreFragment())
//            }
//            true
//        }
    }

    private fun showFragment(fragment: Fragment) {
        // FragmentManager
        val fragmentManager: FragmentManager = supportFragmentManager

        // FragmentTransaction
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()

        // Replace fragment in lytFragment
        fragmentTransaction.replace(R.id.lytFragment, fragment)

        // Commit transaction
        fragmentTransaction.commit()
    }

}



@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    EBookCambodiaTheme {
        Greeting("Android")
    }
}