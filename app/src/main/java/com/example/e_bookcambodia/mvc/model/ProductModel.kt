package com.example.e_bookcambodia.mvc.model

class ProductModdel {
    private var id = 0
    private var image_product: String? = null
    private var name_product: String? = null
    private var des_product: String? = null
    private var price_product = 0
    private var rating_product = 0
    private var created_at: String? = null
    private var updated_at: String? = null

    fun getId(): Int {
        return id
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun getImage_product(): String? {
        return image_product
    }

    fun setImage_product(image_product: String?) {
        this.image_product = image_product
    }

    fun getName_product(): String? {
        return name_product
    }

    fun setName_product(name_product: String?) {
        this.name_product = name_product
    }

    fun getDes_product(): String? {
        return des_product
    }

    fun setDes_product(des_product: String?) {
        this.des_product = des_product
    }

    fun getPrice_product(): Int {
        return price_product
    }

    fun setPrice_product(price_product: Int) {
        this.price_product = price_product
    }

    fun getRating_product(): Int {
        return rating_product
    }

    fun setRating_product(rating_product: Int) {
        this.rating_product = rating_product
    }

    fun getCreated_at(): String? {
        return created_at
    }

    fun setCreated_at(created_at: String?) {
        this.created_at = created_at
    }

    fun getUpdated_at(): String? {
        return updated_at
    }

    fun setUpdated_at(updated_at: String?) {
        this.updated_at = updated_at
    }
}