package com.example.e_bookcambodia.mvc.controller

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.e_bookcambodia.mvc.model.ProductModdel
import com.example.e_bookcambodia.databinding.ViewHolderStoreBinding
import com.squareup.picasso.Picasso

class StoreAdapter :
    ListAdapter<ProductModdel, StoreAdapter.StoreViewHolder>(ProductDiffCallback())  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewHolderStoreBinding =
            ViewHolderStoreBinding.inflate(layoutInflater, parent, false)
        return StoreViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val products: ProductModdel? = getItem(position)
        holder.bind(products)

    }

    class StoreViewHolder(productsBinding: ViewHolderStoreBinding) :
        RecyclerView.ViewHolder(productsBinding.getRoot()) {
        private val productsBinding: ViewHolderStoreBinding

        init {
            this.productsBinding = productsBinding
        }

        fun bind(products: ProductModdel?) {
            Picasso.get().load(products!!.getImage_product()).into(productsBinding.imageProduct)
            productsBinding.nameProduct.setText(products.getName_product())
            productsBinding.desProduct.setText(products.getDes_product())

        }
    }

    class ProductDiffCallback : DiffUtil.ItemCallback<ProductModdel>() {
        override fun areItemsTheSame(oldItem: ProductModdel, newItem: ProductModdel): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: ProductModdel, newItem: ProductModdel): Boolean {
            return oldItem.getId() == newItem.getId()
        }
    }
}