package com.example.e_bookcambodia.mvc.view.fragment


import androidx.fragment.app.Fragment

import androidx.fragment.app.FragmentManager

import androidx.fragment.app.FragmentTransaction

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.recyclerview.widget.GridLayoutManager
import com.example.e_bookcambodia.mvc.model.ProductModdel
import com.example.e_bookcambodia.service.ApiServices
import com.example.e_bookcambodia.databinding.FragmentStoreBinding
import com.example.e_bookcambodia.mvc.controller.StoreAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class StoreFragment : Fragment() {
    private lateinit var binding : FragmentStoreBinding

    override fun onCreateView(
        @NonNull inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =FragmentStoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadProductsListFromServer()
    }

    private fun loadProductsListFromServer() {
        val httpClient = Retrofit.Builder()
            .baseUrl("https://ebooks-store-cambo.000webhostapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val apiServices: ApiServices = httpClient.create(ApiServices::class.java)

        val task: Call<List<ProductModdel?>?>? = apiServices.loadProductsList()

        task!!.enqueue(object : Callback<List<ProductModdel?>?> {
            override fun onResponse(
                call: Call<List<ProductModdel?>?>,
                response: Response<List<ProductModdel?>?>
            ) {
                if (response.isSuccessful) {
                    showProductsList(response.body())
                } else {
                    Toast.makeText(context, "Products List received!", Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<List<ProductModdel?>?>, t: Throwable) {
                Toast.makeText(context, "Load products list failed!", Toast.LENGTH_LONG).show()
                Log.e("[ProductsFragment]", "Load products failed: " + t.message)
            }
        })


    }
    open fun showProductsList(productsList: List<ProductModdel?>?) {
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
//        binding.recyclerviewProducts.setLayoutManager(layoutManager);
        val layoutManager = GridLayoutManager(context, 2)
        binding.recyclerviewStores.setLayoutManager(layoutManager)
//        binding.recyclerviewStores.addItemDecoration(SpacesItemDecoration(10))
        val productsAdapter = StoreAdapter()
        productsAdapter.submitList(productsList)
        binding.recyclerviewStores.setAdapter(productsAdapter)
    }
}