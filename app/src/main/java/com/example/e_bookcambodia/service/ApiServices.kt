package com.example.e_bookcambodia.service

import com.example.e_bookcambodia.mvc.model.ProductModdel
import retrofit2.Call
import retrofit2.http.GET

interface ApiServices {
    @GET("/api/pro/fetch-products")
    fun loadProductsList(): Call<List<ProductModdel?>?>?
}